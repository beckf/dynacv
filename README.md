# dynacv

Dynacv is a free adaption from the work of https://github.com/ddbullfrog/resumecard.

It works with Vue.js and PHP 5.6.

It runs successfully on Chrome and Firefox but not on Internet Explorer.

# Installation

- Composer is required https://getcomposer.org/
- Once installed run the following command : composer install.
- Copy/paste dynacv dir into an htdocs Apache directory with PHP 5.6 enabled. (This step is compulsory in order to create a pdf cv).
- Customize your CV by modifying _table vars available in /model/fr-database.js
- Double click on cv.html 
- Et Voilà ! Enjoy :)

# Fonctionnalities

- It's a Javascript CV based on Vue.js framework,
- It displays a dynamic cv according to a professional profil selected inside the dropbox available at the top page of cv.html file,
- It displays a friendly view for printing,
- You can sort your abilities according to Dreyfus model.