Vue.component('cv-header', {

    template: `
        <div class="container-fluid">
            <div>
                <cv-cartouche></cv-cartouche>
            </div>
            <div class="col-sm-4 visible-md-block visible-lg-block">
                <cv-profil></cv-profil>
            </div>
            <div class="col-sm-4 visible-md-block visible-lg-block visible-sm-block">
                <cv-reseau></cv-reseau>
            </div>
        </div>
    `
});

Vue.component('cv-cartouche', {

    props: ['nom', 'adresse', 'email', 'telephone'],

    template: `
        <div class="col-sm-4"><img src="http://triaubaral.gitlab.io/whoami/assets/img/triaubaral.png" title="Avatar" alt="My Avatar">
        <h4>{{ data.nom }}</h4>
        <p>{{ data.adresse.rue }}<br />{{ data.adresse.ville }}</p>
        <p><a href="mailto:this.text"><i class="fa fa-envelope-o fa-fw"></i>{{ data.email }}</a></p>
        <p>Téléphone : {{ data.telephone }}</p>
        <p>
            <a href="javascript:printCV();" title="Curriculum Vitae" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span> CV</a>
            <a href="http://triaubaral.gitlab.io/whoami/pdf/memoire.pdf" title="Mémoire d'ingénieur" class="btn btn-primary"><span class="fa fa-file-pdf-o"></span>&nbsp;Mémoire</a>
        </p>
        <div class="clearfix"></div>
    `
});

Vue.component('cv-profil', {
    template: `
    <h2><label on="profils">Profil</label>:
        <div class="btn-group bootstrap-select" style="width: 75%;"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" data-id="profils" title="Ingénieur Logiciel"><span class="filter-option pull-left">Ingénieur Logiciel</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox">
            <div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search"></div><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">Ingénieur Logiciel</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Expert Technique</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Développeur Sénior</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Architecte Applicatif</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Leader Technique</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Intégrateur d'application</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="6"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Product Owner</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="7"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Chef de projet</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="8"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Formateur</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select id="profils" data-live-search="true" onchange="newCV(this.value)" data-width="75%" class="selectpicker" tabindex="-98"><option value="moe">Ingénieur Logiciel</option><option value="moe">Expert Technique</option><option value="moe">Développeur Sénior</option><option value="moe">Architecte Applicatif</option><option value="moe">Leader Technique</option><option value="moe">Intégrateur d'application</option><option value="moa">Product Owner</option><option value="moa">Chef de projet</option><option value="moa">Formateur</option></select></div></h2></div>

        `
});

Vue.component('cv-reseau', {
    template: `
            <p>
                <i class="fa fa-github fa-fw"></i>
                <a href="//github.com/triaubaral" target="_blank">@triaubaral</a>
            </p>
            <p>
                <i class="fa fa-linkedin-square fa-fw"></i>
                <a href="//linkedin.com/in/atricoire" target="_blank">linkedin.com/in/atricoire</a>
            </p>
            <p>
                <i class="fa fa-twitter fa-fw"></i>
                <a href="//twitter.com/triaubaral" target="_blank">@triaubaral</a>
            </p>
        `
});

Vue.component('cv-timeline', {
    template: `
        <h3 class="title with-icon"><span class="glyphicon glyphicon-paperclip cat-title"></span>test</h3>
        <ul class="timeline">
            <li class="timeline-inverted" v-for="item in data.experiences_pro">
                <div class="timeline-badge info">{{ item.fin }}</div>
                <div class="timeline-panel grid-block">
                    <div class="timeline-heading">
                        <h4 class="timeline-title"><span style="color:#BA4E4E;font-weight:bolder">{{child.poste.name}}</span> pour {{child.poste.firmName}}</h4>
                    </div>
                    <div class="timeline-body">
                    </div>
                </div>
            </li>
        </ul>
    `
});

Vue.component('cv-timeline-element', {

});

new Vue({
    el: "#app",
    data: data
})
