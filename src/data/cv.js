var data = {
    nom: "BECK Frédéric",
    email: "beck.frederic@gmail.com",
    adresse: {
        rue: "15 chemin fried",
        ville: "67100 STRASBOURG"
    },
    telephone: "06.03.53.51.60",
    experiences_pro: [{
            debut: 2007,
            fin: 2008,
            employeur: "NETIKA",
            ville: "STRASBOURG",
            poste: "Développeur",
            description: null,
            projets: null
        },
        {
            debut: 2008,
            fin: null,
            employeur: "CPAM DU BAS-RHIN",
            ville: "STRASBOURG",
            poste: "Expert technique en développement",
            description: null,
            projets: [{
                annee: 2017,
                nom: "Mise en place d'un réseau social d'entreprise pour les 1000 agents de la caisse",
                tags: ["Gestion de projet", "Benchmarking", "Conduite du changement"]
            }]
        }
    ]
}
